import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  field: {
    color: '#000',
    fontSize: 24,
  },
});

export default styles;
